﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorAtacantes : MonoBehaviour {

    [Tooltip("Número de proyectiles enemigos permitidos en escena")]
    public int proyectilesEnenigosPermitidos = 3;
    [Tooltip("Probabilidad de Disparar (%)")]
    public float chanceDisparo = 50f;

    [Tooltip("Prefab explosión del Atacante")]
    public GameObject explosionAtacante;

    [Tooltip("Arreglo de Sonidos para el Ritmo")]
    public AudioClip[] sonidosRitmo;

    //Esta variable controla la frecuencia de disparo de todos los atacantes
    private bool sePermiteDisparar = true;

    private int score = 0; 
    private int indiceProxSonido, cantidadSonidos;
    private AudioSource aliensAudioSource;
    private GameManager gameManager;

    //Propiedad que permite acceder a la variable privada sePermiteDisparar
    public bool SePermiteDisparar
    {
        get
        {
            return sePermiteDisparar;
        }
    }

    //Suscribirse a los eventos correspondientes
    private void OnEnable()
    {
        MovimientoAtacantes.OnRythmBeat += ReproducirSonidoRitmo;
    }

    //Desuscribirse a los eventos correspondientes
    private void OnDisable()
    {
        MovimientoAtacantes.OnRythmBeat -= ReproducirSonidoRitmo;
    }

    // Use this for initialization
    void Start () {
        indiceProxSonido = 0;
        aliensAudioSource = GetComponent<AudioSource>();
        cantidadSonidos = sonidosRitmo.Length;
        gameManager = FindObjectOfType<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (ProyectilEnemigo.eProyectilCount < proyectilesEnenigosPermitidos)
        {
            sePermiteDisparar = true;
        }
        else
        {
            sePermiteDisparar = false;
        }
    }

    void ReproducirSonidoRitmo()
    {
        aliensAudioSource.clip = sonidosRitmo[indiceProxSonido];
        aliensAudioSource.Play();
        indiceProxSonido++;
        if(indiceProxSonido > cantidadSonidos -1)
        {
            indiceProxSonido = 0;
        }
    }

    public void DestruirAtacante(Atacante miAtacante)
    {
        //Crear la animación de la explosión
        Instantiate(explosionAtacante, miAtacante.transform.position, Quaternion.identity, this.transform);
        //Agregar la puntuación al Score
        score += miAtacante.puntos;
        //Destruir objeto de atacante
        Destroy(miAtacante.gameObject);
        gameManager.MostrarPuntos(score);
    }
}
