﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atacante : MonoBehaviour {

    [Tooltip("Prefab del proyectil del Enemigo")]
    public GameObject proyectilEnemigo;

    [Tooltip("Puntos por destruir éste Atacante")]
    public int puntos = 0;

    [Tooltip("Arreglo con los Sprites que conforman la animación del Atacante")]
    public Sprite[] misSprites;

    //Esta variable se utilizará para acumular el tiempo desde el último disparo
    private float tiempoUltimoDisparo = 0;
    private float tiempoProximoDisparo = 0;

    //Estas variables se utilizarán para acceder a los sprites del alien
    private SpriteRenderer mySpriteRenderer;
    private int cantidadSprites, currentSpriteIndex;

    private ControladorAtacantes miControladorAtacantes;
    private MovimientoAtacantes elMovimientoDeMisAtacantes;

    private bool sePuedeDisparar = false;
    
    //Suscribirse a los eventos correspondientes
    private void OnEnable()
    {
        AliensSpawner.OnAliensSpawned += ActivarDisparos;
        MovimientoAtacantes.OnRythmBeat += AnimarSprite;
        MovimientoAtacantes.OnRythmBeat += ChangeDirection;
        #region Suscripción a los eventos de destrucción y reactivación del Jugador
        GameManager.OnPlayerDestroyed += DetenerDisparos;
        GameManager.OnPlayerRevived += ActivarDisparos;
        #endregion
    }

    //Desuscribirse de los eventos correspondientes
    private void OnDisable()
    {
        AliensSpawner.OnAliensSpawned -= ActivarDisparos;
        MovimientoAtacantes.OnRythmBeat -= AnimarSprite;
        MovimientoAtacantes.OnRythmBeat -= ChangeDirection;
        #region Desubscripción de los eventos de destrucción y reactivación del Jugador
        GameManager.OnPlayerDestroyed -= DetenerDisparos;
        GameManager.OnPlayerRevived -= ActivarDisparos;
        #endregion
    }

    // Use this for initialization
    void Start () {
        miControladorAtacantes = FindObjectOfType<ControladorAtacantes>();
        elMovimientoDeMisAtacantes = miControladorAtacantes.gameObject.GetComponent<MovimientoAtacantes>();

        SetNextShootTime();

        //Esto inicializa todo lo relacionado con los Sprites del atacante
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        currentSpriteIndex = 0;
        mySpriteRenderer.sprite = misSprites[currentSpriteIndex];
        cantidadSprites = misSprites.Length;

    }

    // Update is called once per frame
    void Update()
    {
        if (sePuedeDisparar)
        {
            tiempoUltimoDisparo += Time.deltaTime;
            //Verificar que se alcanzó el tiempo para el próximo disparo y si la variable de control no es verdadera
            if (tiempoUltimoDisparo >= tiempoProximoDisparo)
            {
                if (miControladorAtacantes.SePermiteDisparar)
                {
                    Disparar();
                }
                ReiniciarConteo();
            }
        }
    }

    public void SetNextShootTime()
    {
        tiempoProximoDisparo = Random.Range(0.5f, 3f);
    }

    void Disparar()
    {
        //Si se da el chance disparar
        if (MisFunciones.Chance(miControladorAtacantes.chanceDisparo))
        {
            //Generar el proyectil
            Instantiate(proyectilEnemigo, transform.position, Quaternion.identity);
        }
    }

    public void ReiniciarConteo()
    {
        tiempoUltimoDisparo = 0;
        SetNextShootTime();
    }

    private void AnimarSprite()
    {
        currentSpriteIndex++;
        if (currentSpriteIndex > cantidadSprites - 1)
        {
            currentSpriteIndex = 0;
        }
        mySpriteRenderer.sprite = misSprites[currentSpriteIndex];
    }

    public float CheckBorderDistance()
    {
        float distancia;
        float bordeX = elMovimientoDeMisAtacantes.ProximaDireccion.x;
        distancia = Mathf.Abs((bordeX * MisFunciones.CoordenadaXPantalla()) - transform.position.x - bordeX * mySpriteRenderer.bounds.size.x/2);
        return distancia;
    }

    public void ChangeDirection()
    {
        if(CheckBorderDistance() < elMovimientoDeMisAtacantes.distanciaPasoHorizontal)
        {
            Vector3 pos = elMovimientoDeMisAtacantes.ProximaDireccion;
            pos.x *= -1;
            elMovimientoDeMisAtacantes.ProximaDireccion = pos;
        }
    }

    void ActivarDisparos()
    {
        sePuedeDisparar = true;
    }

    #region Nueva Función para detener disparos de los atacantes
    void DetenerDisparos()
    {
        sePuedeDisparar = false;
    }
    #endregion  
}
