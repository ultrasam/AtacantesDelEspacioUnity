﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoAtacantes : MonoBehaviour {

    [Tooltip("Representa el ritmo del juego en segundos")]
    public float ritmoJuego = 2;
    [Tooltip("Indica si los atacantes se deben mover o no")]
    public bool ritmoActivo = false;
    [Tooltip("Indica que tanto se mueven horizontalmente los atacantes")]
    public float distanciaPasoHorizontal = 0.5f;
    [Tooltip("Indica que tanto se mueven verticalmente los atacantes")]
    public float distanciaPasoVertical = 1f;

    //Creación de Evento que se dispara en cada paso del ritmo
    //Primero creamos el delegado
    public delegate void AccionRitmo();
    //Luego creamos el evento
    public static event AccionRitmo OnRythmBeat;

    //Variable utilizada para acumular el tiempo transcurrido
    private float acumuladorTiempo = 0;

    private Vector3 proximaDireccion, direccionAnterior;

    //Suscribirse a los eventos correspondientes
    private void OnEnable()
    {
        AliensSpawner.OnAliensSpawned += ActivarMovimiento;
        #region Suscripción a los eventos de destrucción y reactivación del Jugador
        GameManager.OnPlayerDestroyed += DesactivarMovimiento;
        GameManager.OnPlayerRevived += ActivarMovimiento;
        #endregion
    }

    //Desuscribirse de los eventos correspondientes
    private void OnDisable()
    {
        AliensSpawner.OnAliensSpawned -= ActivarMovimiento;
        #region Desubscripción de los eventos de destrucción y reactivación del Jugador
        GameManager.OnPlayerDestroyed -= DesactivarMovimiento;
        GameManager.OnPlayerRevived -= ActivarMovimiento;
        #endregion
    }

    // Use this for initialization
    void Start () {
		if(MisFunciones.Chance(50))
        {
            proximaDireccion = Vector3.right;
        } else
        {
            proximaDireccion = Vector3.left;
        }
        direccionAnterior = proximaDireccion;
        ritmoActivo = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (ritmoActivo)
        {
            acumuladorTiempo += Time.deltaTime;
        }
		
        if(acumuladorTiempo >= ritmoJuego)
        {
            Vector3 pos = transform.position;
            //Verificar se la dirección cambió
            if(direccionAnterior != proximaDireccion)
            {
                //Mover los atacantes hacia abajo
                pos += Vector3.down * distanciaPasoVertical;
            } else
            {
                pos += proximaDireccion * distanciaPasoHorizontal;
            }
            transform.position = pos;
            direccionAnterior = proximaDireccion;
            //Disparar Evento
            if (OnRythmBeat != null)
            {
                OnRythmBeat();
            }
            //Reiniciar Acumulador
            acumuladorTiempo = 0;
        }
    }

    public Vector3 ProximaDireccion
    {
        get
        {
            return proximaDireccion;
        }
        set
        {
            proximaDireccion = value;
        }
    }

    void ActivarMovimiento()
    {
        ritmoActivo = true;
    }

    #region Nueva Función para Desactivar el Movimiento de los Aliens
    void DesactivarMovimiento()
    {
        ritmoActivo = false;
    }
    #endregion
}
