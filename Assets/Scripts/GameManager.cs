﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    [Tooltip("Prefab de imágen UI vidas del Jugador")]
    public GameObject iconoVidaJugador;
    [Tooltip("Cantidad de vidas iniciales del Jugador")]
    public int vidasJugador = 3;
    [Tooltip("Objeto Canvas del UI")]
    public GameObject canvasUI;
    [Tooltip("Prefab explosión Jugador")]
    public GameObject explosionJugador;
    #region Agregar nuevo campo para cambiar el tiempo entre muertes del jugador
    [Tooltip("Tiempo entre muertes del Jugador")]
    [SerializeField] float tiempoEntreMuertes = 2f;
    #endregion
    private GameObject panelVidas, panelPuntos, miJugador;

    #region Creación de Eventos que se disparan al destruir o reactivar el jugador
    //Primero creamos el delegado
    public delegate void AccionJugador();
    //Luego creamos los eventos
    public static event AccionJugador OnPlayerDestroyed;
    public static event AccionJugador OnPlayerRevived;
    #endregion

    //Suscribirse a los eventos correspondientes
    private void OnEnable()
    {
        AliensSpawner.OnAliensSpawned += ActivarAccionesJugador;
    }

    //Desuscribirse de los eventos correspondientes
    private void OnDisable()
    {
        AliensSpawner.OnAliensSpawned -= ActivarAccionesJugador;
    }

    // Use this for initialization
    void Start () {
        //encontrar referencia del objeto jugador
        miJugador = GameObject.FindGameObjectWithTag("Player");
        panelVidas = canvasUI.transform.Find("PanelVidas").gameObject;
        panelPuntos = canvasUI.transform.Find("PanelPuntos").gameObject;
        DibujarVidas(vidasJugador);
        MostrarPuntos(0);
        DesactivarAccionesJugador();
    }

    // Update is called once per frame
    void Update () {
		
	}

    void DibujarVidas (int cantVidas)
    {
        for (int i = 0; i < cantVidas; i++)
        {
            Instantiate(iconoVidaJugador, panelVidas.transform);
        }
    }

    void BorrarVidas()
    {
        foreach (Transform myTransform in panelVidas.transform)
        {
            Destroy(myTransform.gameObject);
        }
    }

    public void MostrarPuntos(int puntosAMostrar)
    {
        Text myText = panelPuntos.transform.GetChild(0).GetComponent<Text>();
        myText.text = puntosAMostrar.ToString();
    }

    public void DestruirJugador()
    {
        StartCoroutine(DestruccionJugador());
    }

    IEnumerator DestruccionJugador()
    {
        //Desaparecer/Desactivar Jugador
        ActivarDesactivarJugador(false);
        //Instanciar explosión Jugador
        Instantiate(explosionJugador, miJugador.transform.position, Quaternion.identity);
        //Restar 1 a las vidas
        vidasJugador--;
        BorrarVidas();
        if (vidasJugador > 0)
        {
            //Redibujar las vidas en el UI
            DibujarVidas(vidasJugador);
            #region Esperar un tiempo antes de regenerar al Jugador (este tiempo estaba fijo en 2f, pero ahora es la variable tiempoEntreMuertes
            yield return new WaitForSeconds(tiempoEntreMuertes);
            #endregion
            ActivarDesactivarJugador(true);
        }
        else
        {
            //Significa que no hay más vidas
            //GAME OVER
        }
    }

    private void ActivarDesactivarJugador(bool activar)
    {
        //Desaparecer el Jugador de la escena
        miJugador.GetComponent<SpriteRenderer>().enabled = activar;
        //Evitar que se pueda seguir moviendo el jugador
        miJugador.GetComponent<MovimientoJugador>().enabled = activar;
        //Evitar que pueda seguir disparando
        miJugador.GetComponent<DisparoJugador>().enabled = activar;
        //Evitar que siga detectando colisiones
        miJugador.GetComponent<BoxCollider2D>().enabled = activar;
        #region Disparar eventos OnPlayerRevived u OnPlayerDestroyed dependiendo de si se activa o desactiva el Jugador
        if (activar)
        {
            //Disparar Evento Jugador Reactivado
            if (OnPlayerRevived!= null)
            {
                OnPlayerRevived();
            }
        }
        else
        {
            //Disparar Evento Jugador Destruido
            if (OnPlayerDestroyed != null)
            {
                OnPlayerDestroyed();
            }
        }
        #endregion
    }

    void DesactivarAccionesJugador()
    {
        miJugador.GetComponent<MovimientoJugador>().enabled = false;
        miJugador.GetComponent<DisparoJugador>().enabled = false;
        miJugador.GetComponent<BoxCollider2D>().enabled = false;
    }

    void ActivarAccionesJugador()
    {
        miJugador.GetComponent<MovimientoJugador>().enabled = true;
        miJugador.GetComponent<DisparoJugador>().enabled = true;
        miJugador.GetComponent<BoxCollider2D>().enabled = true;
    }
}