﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoJugador : MonoBehaviour {

    [Tooltip("Velocidad del Jugador")]
    public float velocidadJugador = 1;

    private float screenBorder, maxPosition, cannonWidth;

    // Use this for initialization
    void Start () {

        //Obtener la posición del borde derecho de la pantalla en unidades unity
        screenBorder = MisFunciones.CoordenadaXPantalla();
        //Obtener el ancho del sprite del jugador en unidades Unity
        cannonWidth = GetComponent<SpriteRenderer>().bounds.size.x;

        maxPosition = screenBorder-(cannonWidth/2);
    }

    // Update is called once per frame
    void Update () {
        //Mover el transform de este objeto al activar el eje horizontal
        transform.Translate(Vector3.right * Input.GetAxisRaw("Horizontal") * velocidadJugador * Time.deltaTime);
        //restringir el movimiento del jugador a la posición máxima a la derecha y a la izquierda
        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, -maxPosition, maxPosition);
        transform.position = pos;
    }
}
