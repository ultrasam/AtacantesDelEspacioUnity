﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AliensSpawner : MonoBehaviour {

    [System.Serializable]
    public struct CantidadFilasDeAtacante
    {
        public GameObject miAtacante;
        public int numeroFilas;

        //Constructor (no es necesario pero de mucha ayuda)
        public CantidadFilasDeAtacante(GameObject elAtacante, int cuantasFilas)
        {
            miAtacante = elAtacante;
            numeroFilas = cuantasFilas;
        }
    }

    [Tooltip("Prefabs de Aliens y #Filas de abajo hacia arriba")]
    public CantidadFilasDeAtacante[] cantidadFilasDeAtacantes;
    [Tooltip("Número de columnas de Aliens")]
    public int numeroColumnas;
    [Tooltip("Tiempo de creación de los Atacantes")]
    public float tiempoCreacion;

    public float espaciamientoMax = 1;

    private float espaciamientoMin = 1;

    //Creación de Evento que se dispara al culminar la creación de los atacantes
    //Primero creamos el delegado
    public delegate void AccionSpawner();
    //Luego creamos el evento
    public static event AccionSpawner OnAliensSpawned;

    // Use this for initialization
    void Start () {
        float tiempoEntreAliens = tiempoCreacion / (((float)CalcularTotalFilas() * (float)numeroColumnas));
        StartCoroutine(CrearAtacantes(tiempoEntreAliens));
    }
    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator CrearAtacantes(float tiempoSpawn)
    {
        //Capturo el tiempo Original esperado para poder usarlo más adelante para comparar contra el real
        float originalSpawnTime = tiempoSpawn;

        //Para evitar que un alien quede encima de otro el espaciamiento mínimo 
        //será el ancho del alien más ancho (el primero del arreglo)
        espaciamientoMin = cantidadFilasDeAtacantes[0].miAtacante.GetComponent<SpriteRenderer>().size.x;

        //Calcular espacio horizontal disponible en la pantalla quitando mitad del ancho del alien en ambos lados 
        float espacioDisponible = (MisFunciones.CoordenadaXPantalla() * 2) - espaciamientoMin;

        //Para evitar más columnas de las que caben en el espacio disponible, le hago Clamp al número de columnas
        int maxNumeroColumnas = Mathf.RoundToInt(espacioDisponible / espaciamientoMin);
        numeroColumnas = Mathf.Clamp(numeroColumnas, 1, maxNumeroColumnas);

        //En base al número de columnas y el espacio disponible se calcula el espaciamiento entre aliens
        //el valor resultante siempre debe estar entre el espaciamiento máximo y el mínimo
        float espaciamientoEntreAliens = espacioDisponible / (float)numeroColumnas;
        espaciamientoEntreAliens = Mathf.Clamp(espaciamientoEntreAliens, espaciamientoMin, espaciamientoMax);

        //Calcular el x inicial a la izquierda
        float xInicial = ((float)numeroColumnas / 2f) * espaciamientoEntreAliens - (espaciamientoEntreAliens / 2f);

        //Obtener el número total de filas
        int numeroTotalFilas = CalcularTotalFilas();

        //Calcular el y inicial, empezando por abajo (por eso es negativo)
        float yInicial = -((float)numeroTotalFilas / 2f) * espaciamientoEntreAliens + (espaciamientoEntreAliens / 2f);

        //Aquí instanciamos los atacantes iterando primero por cada miembro del arreglo (struct)
        foreach (CantidadFilasDeAtacante miCantidadDeAtacante in cantidadFilasDeAtacantes)
        {
            //luego iteramos por cada fila indicada en el struct con la variable numeroFilas
            for (int j = 0; j < miCantidadDeAtacante.numeroFilas; j++)
            {
                //luego iteramos de una en una cada columna para así instanciar cada atacante correspondiente
                for (int i = 0; i < numeroColumnas; i++)
                {
                    //acá instanciamos el atacante de la columna actual asignándole como padre al objeto Aliens
                    GameObject miAtacante = Instantiate(miCantidadDeAtacante.miAtacante, this.transform);
                    //y ahora cambiamos su posición local (en relación al padre)
                    //(i * espaciamientoEntreAliens) nos permite en cada iteración del ciclo for colocar el nuevo alien en la siguiente
                    //columna a la derecha. El -xInicial nos permite moverlo a su posición correcta a la izquierda para que quede centrado.
                    //el yInicial corresponde a la coordenada Y la cual es la misma para toda fila.
                    miAtacante.transform.localPosition = new Vector3((i * espaciamientoEntreAliens) - xInicial, yInicial, 0);
                    float timeA = Time.time;
                    yield return new WaitForSeconds(tiempoSpawn);
                    float timeB = Time.time;
                    if ((timeB - timeA) > originalSpawnTime)
                    {
                        tiempoSpawn -= Time.deltaTime / 5;
                    }
                    else if ((timeB - timeA) < originalSpawnTime)
                    {
                        tiempoSpawn += Time.deltaTime / 5;
                    }

                }
                //al terminar de colocar cada fila le sumamos a yInicial el espaciamiento para poder instanciar la siguiente fila
                yInicial += espaciamientoEntreAliens;
            }
        }
        //Disparar Evento
        if (OnAliensSpawned != null)
        {
            OnAliensSpawned();
        }
    }

    int CalcularTotalFilas()
    {
        int numeroTotalFilas = 0;
        foreach (CantidadFilasDeAtacante miCantidadDeAtacante in cantidadFilasDeAtacantes)
        {
            numeroTotalFilas += miCantidadDeAtacante.numeroFilas;
        }
        return numeroTotalFilas;
    }
}