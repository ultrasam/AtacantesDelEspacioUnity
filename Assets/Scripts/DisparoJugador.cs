﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoJugador : MonoBehaviour {

    [Tooltip("Número de proyectiles permitidos en escena")]
    public int proyectilesPermitidos = 1;
    [Tooltip("Tiempo mínimo entre cada disparo (segundos)")]
    public float fireRate = 1f;
    [Tooltip("Prefab del proyectil del Jugador")]
    public GameObject proyectilJugador;
    [Tooltip("Sonido del proyectil del Jugador")]
    public AudioClip sonidoDisparo;

    //Esta variable controla la frecuencia de disparo
    private bool sePuedeDisparar = true;
    //Esta variable se utilizará para acumular el tiempo desde el último disparo
    private float tiempoUltimoDisparo = 0;

    private AudioSource myAudioSource;

    // Use this for initialization
    void Start () {
        myAudioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton("Fire1") && sePuedeDisparar)
        {
            Disparar();
        }
        if(!sePuedeDisparar)
        {
            tiempoUltimoDisparo += Time.deltaTime;
        }
        //Reiniciar el acumulador y la variable de control al llegar al fireRate
        if(tiempoUltimoDisparo >= fireRate)
        {
            tiempoUltimoDisparo = 0;
            sePuedeDisparar = true;
        }
	}

    void Disparar()
    {
        if(ProyectilJugador.jProyectilCount < proyectilesPermitidos)
        {
            //Generar el Proyectil
            Instantiate(proyectilJugador, transform.position, Quaternion.identity);
            //Reproducir el sonido
            myAudioSource.clip = sonidoDisparo;
            myAudioSource.Play();
            sePuedeDisparar = false;
        }
    }
}
