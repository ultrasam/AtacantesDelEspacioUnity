﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuerteJugador : MonoBehaviour {

    private GameManager myGameManager;
    // Use this for initialization
    void Start()
    {
        myGameManager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("ProyectilEnemigo"))
        {
            //Destruir Jugador
            myGameManager.DestruirJugador();
            //Destruir el proyectil de la colisión
            Destroy(collision.gameObject);
        }
    }
}
