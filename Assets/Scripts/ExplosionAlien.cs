﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionAlien : MonoBehaviour {

    private AudioSource myAudioSource;
	// Use this for initialization
	void Start () {
        myAudioSource = GetComponent<AudioSource>();
        Destroy(gameObject, myAudioSource.clip.length);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
