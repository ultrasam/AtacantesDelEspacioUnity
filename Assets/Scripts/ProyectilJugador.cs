﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilJugador : MonoBehaviour
{

    [Tooltip("Velocidad del Proyectil")]
    public float velocidadMovimiento = 6;

    public static int jProyectilCount = 0;

    private ControladorAtacantes miControladorAtacantes;

    // Use this for initialization
    void Start()
    {
        jProyectilCount++;
        miControladorAtacantes = FindObjectOfType<ControladorAtacantes>();
    }

    private void OnDestroy()
    {
        jProyectilCount--;
    }
    // Update is called once per frame
    void Update()
    {
        //Mover el transform de este objeto hacia arriba
        transform.Translate(Vector3.up * velocidadMovimiento * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Atacante"))
        {
            //Destruir Atacante ya que solo tienen una vida
            miControladorAtacantes.DestruirAtacante(collision.gameObject.GetComponent<Atacante>());
            //Destruir el proyectil de la colisión
            Destroy(gameObject);
        }
        if (collision.gameObject.CompareTag("Barrera"))
        {
            //Destruir pixel colisionado
            Destroy(collision.gameObject);
            //Destruir el proyectil de la colisión
            Destroy(gameObject);
        }
    }
}