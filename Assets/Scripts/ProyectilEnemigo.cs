﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProyectilEnemigo : MonoBehaviour {
    [Tooltip("Velocidad del Proyectil")]
    public float velocidadMovimiento = 5;
    public static int eProyectilCount = 0;


    // Use this for initialization
    void Start()
    {
        eProyectilCount++;
    }

    private void OnDestroy()
    {
        eProyectilCount--;
    }
    // Update is called once per frame
    void Update()
    {
        //Mover el transform de este objeto hacia abajo
        transform.Translate(Vector3.down * velocidadMovimiento * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Barrera"))
        {
            //Destruir pixel colisionado
            Destroy(collision.gameObject);
            //Destruir el proyectil de la colisión
            Destroy(gameObject);
        }

    }
}
