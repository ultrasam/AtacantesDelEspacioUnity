﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MisFunciones {

    //Regresa verdadero o falso dependiendo del porcentaje de probabilidad
    public static bool Chance(float porcentaje)
    {
        bool resultado = false;
        float valorMaximo = 100f;
        //Aquí se selecciona un valor aleatorio
        float chance = Random.Range(0f, valorMaximo);
        //Mientras mayor sea el porcentaje, mayor será la probabilidad de que el resultado sea verdadero
        if (chance <= (valorMaximo * (porcentaje / 100)))
        {
            resultado = true;
        }

        return resultado;

    }

    //Regresa el valor en unidades unity de la coordenada X del borde de la pantalla (como el origen está en el centro positivo es borde derecho y negativo es el izquierdo)
    public static float CoordenadaXPantalla()
    {
        return Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0)).x;
    }

}
